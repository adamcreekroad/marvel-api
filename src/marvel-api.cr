require "http"
require "json"
require "digest"

require "./marvel/**"

module Marvel
  def self.configuration
    @@configuration ||= Configuration.new
  end

  def self.configure
    yield configuration
  end
end
