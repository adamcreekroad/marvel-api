module Marvel
  class Series < Resource
    api_path series

    has_resource characters : Character
    has_resource comics : Comic
    has_resource creators : Creator
    has_resource events : Event
    has_resource stories : Story
  end
end
