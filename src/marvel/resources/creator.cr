module Marvel
  class Creator < Resource
    api_path creators

    has_resource comics : Comic
    has_resource events : Event
    has_resource series : Series
    has_resource stories : Story
  end
end
