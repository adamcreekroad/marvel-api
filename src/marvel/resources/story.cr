module Marvel
  class Story < Resource
    api_path stories

    has_resource characters : Character
    has_resource comics : Comic
    has_resource creators : Creator
    has_resource events : Event
    has_resource series : Series
  end
end
