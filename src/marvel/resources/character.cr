module Marvel
  class Character < Resource
    api_path characters

    has_resource comics : Comic
    has_resource events : Event
    has_resource series : Series
    has_resource stories : Story
  end
end
