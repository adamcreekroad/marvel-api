module Marvel
  class Comic < Resource
    api_path comics

    has_resource characters : Character
    has_resource creators : Creator
    has_resource events : Event
    has_resource stories : Story
  end
end
