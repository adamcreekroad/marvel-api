module Marvel
  class Event < Resource
    api_path events

    has_resource characters : Character
    has_resource comics : Comic
    has_resource creators : Creator
    has_resource series : Series
    has_resource stories : Story
  end
end
