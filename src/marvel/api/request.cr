module Marvel
  module API
    class Request
      V1_ENDPOINT = "/v1/public"

      property params : String
      property headers : HTTP::Headers
      property response : HTTP::Client::Response

      getter parsed_response : JSON::Any

      def initialize(@method : String, path : String, params : Hash(String, String), headers : Hash(String, String))
        @__client = HTTP::Client.new(API::HOST)

        @uri     = "#{V1_ENDPOINT}#{path}"
        @params  = build_params(params)
        @headers = build_headers(headers)

        @response        = @__client.get("#{@uri}?#{@params}", @headers)
        @parsed_response = JSON.parse(@response.body)
      end

      protected def build_params(params)
        HTTP::Params.encode(auth_params.merge(params))
      end

      protected def build_headers(headers)
        __headers = HTTP::Headers.new

        __headers["Accept"]    = "application/json"
        __headers["UserAgent"] = "marvel-api.cr"

        headers.each { |key, value| __headers[key] = value }

        __headers
      end

      def auth_params(ts = Time.utc.to_unix.to_s)
        { "ts" => ts, "apikey" => public_key, "hash" => generate_auth_hash(ts) }
      end

      protected def generate_auth_hash(ts)
        Digest::MD5.hexdigest("#{ts}#{private_key}#{public_key}")
      end

      protected def public_key
        Marvel.configuration.public_key || ""
      end

      protected def private_key
        Marvel.configuration.private_key || ""
      end
    end
  end
end
