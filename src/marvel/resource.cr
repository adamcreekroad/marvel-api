module Marvel
  class Resource
    getter id : Int32
    getter json_attributes : JSON::Any

    macro api_path(path)
      protected def self.api_path
        "/{{ path }}"
      end

      protected def api_path
        "/{{ path }}"
      end
    end

    macro has_resource(resource)
      {% name = resource.id.split(" : ")[0] %}
      {% klass = resource.id.split(" : ")[1] %}

      def {{ name.id }}(params = {} of String => String)
        {{ klass.id }}.all_within("#{api_path}/#{id}", params)
      end
    end

    def self.all(params = {} of String => String)
      list_all(api_path, params)
    end

    def self.all_within(url, params = {} of String => String)
      list_all("#{url}#{api_path}", params)
    end

    def self.get(id)
      list_one("#{api_path}/#{id}")
    end

    def initialize(@json_attributes)
      @id = @json_attributes["id"].as_i
    end

    def [](key)
      @json_attributes[key]
    end

    def as_json
      @json_attributes
    end

    protected def self.list_all(url, params)
      request = API.get(url, params)

      return [] of self unless request.parsed_response["data"]?

      request.parsed_response["data"]["results"].as_a.map { |result| new(result) }
    end


    protected def self.list_one(url)
      request = API.get(url)

      return unless request.parsed_response["data"]?

      new(request.parsed_response["data"]["results"][0])
    end
  end
end
