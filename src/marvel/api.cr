module Marvel
  module API
    HOST = "gateway.marvel.com"

    def self.request(method, uri, params, headers)
      Request.new(method, uri, params, headers)
    end

    def self.get(uri, params = {} of String => String, headers = {} of String => String)
      request("get", uri, params, headers)
    end
  end
end
